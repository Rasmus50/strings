#include <stdio.h>
#include <stdlib.h>

void stringcpy(char *dest, const char *src);
char *stringdup(const char *str);
int stringlen(const char *str);
int stringcmp(char *str1, char *str2);
char *stringcat(char *dest, const char *src);
char *stringcatn(char *dest, const char *src, int size);
char *stringchr(const char *str, int ch);
char *stringchrr(const char *str, int ch);
char *stirfry(const char *str);
char *stringrev(const char *in);
size_t stringspn(const char *s, const char *accept);
size_t stringcspn(const char *s, const char *reject);

size_t stringspn(const char *s, const char *accept) {

	size_t len = 0;

	while(*s != '\0' && *s != *accept)
		s++;
	if(*s == '\0')
		return len;
	while(*s++ == *accept)
		len++;

	return len;
}

size_t stringcspn(const char *s, const char *reject) {

	size_t len =0; 

	while(*s++ != '\0' && *s++ != *reject) {
		len++;
	}
	return len;
}

char *stringrev(const char *in) {

	char *rev = malloc(stringlen(in));
	char *ptr;

	if(!rev) {
		perror("calloc");
		return NULL;
	}
	ptr = stringchr(in, '\0');
	ptr--;
	for(int i = 0; i < (stringlen(in) + 1); i++) {
		rev[i] = *ptr;
		ptr--;
	}
	return rev;
}

char* stringchr(const char* str, int ch) {

	/* going through each character of string and compare each to specified value */
	do{
		if(ch == *str) {
			return (char *)str;
			/* once the value is found break from the loop */
		}
	}while(*str++);
	/* if the character is not found return NULL */
	return NULL;
}

char* stringchrr(const char* str, int ch) {

	/* going through each character of string and compare each to specified value */
	char *ptr;
	do{
		if(ch == *str) {
			ptr = (char *)str;
			/* once the value is found break from the loop */
		}
	}while(*str++);

	if(!ptr)
		return NULL;
	else { 
		return ptr;
	}
}

char *stringcatn(char *dest, const char *src, int size) {

	/* concatenate destination string from source using the amount of bytes specified by size */

	char* ptr;
	int n = 0;

	/* assign pointer to end of source string to ptr */
	while(*ptr != '\0') {
		*ptr++;
	}
	/* increment pointer and overwrite the value with derefernced source pointer, keep n lower than size arg */
	while((*ptr++ = *src++) && n++ < size - 1);
	return dest;
}
		
char* stringcat(char *dest, const char *src) {

	/* concatenate destination string with source string */

	char* ptr;
	
	/* assign pointer to end of source string to ptr */
	ptr = stringchr(dest, '\0');
	while(*ptr++ = *src++);
	return dest;
}

int stringlen(const char* str) {

	/* find the length of a string */

	int len = 0;

	while(*str != '\0') {
		*str++;
		len++;
	}
	return len;
}

char* stringdup(const char* str) {

	/* duplicate a string a return a pointer to the duplicate */

	char* dup;
	
	if((dup = malloc(stringlen(str))) == NULL) {
		perror("calloc");
		exit(1);
	}
	stringcpy(dup, str);
	return dup;
}

void stringcpy(char* dest, const char* src) {

	/* copy the source string to the destination string */

	while(*dest++ = *src++);
}

void stringcpyn(char* dest, char* src, int size) {

	/* copy <size> bytes from source string to destination string */

	int n = 0;

	while((*dest++ = *src++) && n++ < size -1);
}

int stringcmp(char* str1, char* str2) {

	//fix stringcmp
	int n = 0;

	while(*str1 == *str2) {
		n++;
		if(*str1++ != *str2++) {
			return n;
		}
	}
	if(stringlen(str1) == stringlen(str2) == n) {
		return 0;
	}
	else if(stringlen(str1) < n) {
		return n - (2 * n);
	}
	else {
		return n;
	}
	return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include "diystrings.h"

int main(int argc, char **argv) { 

	char *str1;
	char *str2;

	if((str1 = calloc(30, 1)) == NULL) {
		perror("calloc");
		exit(1);
	}
	if((str2 = calloc(30, 1)) == NULL) {
		perror("calloc");
		exit(1);
	}
	
	if(argc != 3) {
		printf("Usage: string_demo <string1> <string2>\n");
		exit(2);
	}

	stringcpy(str1, argv[1]);
	stringcpy(str2, argv[2]);

	printf("%s\n", str1);
	printf("%s\n", str2);

	printf("size of string 1 is %d\n", stringlen(str1));
	printf("size of string 2 is %d\n", stringlen(str2));

	printf("%d\n", stringcmp(str1, str2));

	char *ptr = stringchr(str1, 'a');
	char *ptr1 = stringchrr(str1, 'a');
	
	if(ptr)
		*ptr = 'b';
	printf("returning string 1 after being messed with by stringchr  : %s\n", str1);
	
	if(ptr1)
		*ptr1 = 'e';

	if(ptr1)
		*ptr1 = 'e';
	printf("returning string 1 after being messed with by stringchrr : %s\n", str1);
	printf(" both strings joined together : %s\n", stringcat(str1, str2));
	printf("both strings joined together with a size of 3 : %s\n", stringcatn(str1, str2, 3));


	//printf("stir fry of string 1 : %s\n", stirfry(str1));
	//
	printf("reverse string of str1 : %s\n", stringrev(str1));
	printf("the length of consecutive 'e's in string 1 is : %d\n", stringspn(str1, "e"));
	printf("The length of consequtive characters that aren't 'e' in string 1 is : %d\n", stringcspn(str1, "e"));
	
	exit(0);
}



